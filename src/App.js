import "./App.css";
import Student_Management from "./React_Form/Student_Management";

function App() {
  return (
    <>
      <h1 className="text-center my-4">React Form</h1>
      <Student_Management />
    </>
  );
}

export default App;
