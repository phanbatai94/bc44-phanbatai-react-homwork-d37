import React, { Component } from "react";

export default class Student_List extends Component {
  render() {
    const { data, search, handleDelete, handleStartEdit } = this.props;
    let list = search ? search : data;
    return (
      <div className="mt-2">
        <table className="table">
          <thead>
            <tr className="bg-dark text-white">
              <th scope="col">Mã SV</th>
              <th scope="col">Họ tên</th>
              <th scope="col">Số điện thoại</th>
              <th scope="col">Email</th>
              <th scope="col">Tùy chọn</th>
            </tr>
          </thead>
          <tbody>
            {list.map((item) => {
              return (
                <tr key={item.id}>
                  <th>{item.id}</th>
                  <td>{item.studentName}</td>
                  <td>{item.phoneNumber}</td>
                  <td>{item.email}</td>
                  <td>
                    <button
                      className="btn btn-warning mr-3"
                      onClick={() => handleStartEdit(item.id)}
                    >
                      <i className="fa fa-edit"></i>
                    </button>
                    <button
                      className="btn btn-danger"
                      onClick={() => handleDelete(item.id)}
                    >
                      <i className="fa fa-trash-alt"></i>
                    </button>
                  </td>
                </tr>
              );
            })}
          </tbody>
        </table>
      </div>
    );
  }
}
