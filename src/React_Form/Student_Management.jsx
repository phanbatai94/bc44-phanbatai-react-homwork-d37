import React, { Component } from "react";
import Student_List from "./Student_List";

export default class Student_Management extends Component {
  state = {
    values: {
      id: "",
      studentName: "",
      phoneNumber: "",
      email: "",
    },
    errors: {
      id: "",
      studentName: "",
      phoneNumber: "",
      email: "",
    },
    listStudents: [
      {
        id: "1",
        studentName: "Nguyễn Văn A",
        phoneNumber: "09381111111",
        email: "nguyenvana@gmail.com",
      },
      {
        id: "2",
        studentName: "Nguyễn Văn B",
        phoneNumber: "093822232232",
        email: "nguyenvanb@gmail.com",
      },
    ],
    search: null,
    editingStudent: null,
  };

  handleCheckValid = (event) => {
    const { name, value, pattern } = event.target;
    const newValue = { ...this.state.values, [name]: value };
    const newError = { ...this.state.errors };

    if (!value.trim()) {
      newError[name] = "Trường này không được để trống!";
    } else if (pattern) {
      const regex = new RegExp(pattern);
      const valid = regex.test(value);
      newError[name] = valid ? "" : name + " không đúng định dạng!";
    } else {
      newError[name] = "";
    }

    this.setState({ values: newValue, errors: newError });
  };

  handleSubmit = (event) => {
    event.preventDefault();
    let valid = true;
    let values = this.state.values;
    let newErrors = { ...this.state.errors };

    Object.entries(values).forEach(([name, value]) => {
      if (!value.trim()) {
        newErrors[name] = "Trường này không được để trống!";
        valid = false;
      }
    });

    Object.values(this.state.errors).forEach((item) => {
      if (item.length > 0) {
        valid = false;
      }
    });

    if (valid) {
      const newListStudents = [...this.state.listStudents, this.state.values];
      this.setState({ listStudents: newListStudents });
    } else {
      this.setState({ errors: newErrors });
    }
  };

  handleSearch = (event) => {
    const searchValue = event.target.value;
    const { listStudents: originalListStudents } = this.state;

    let listStudents;
    if (searchValue === "") {
      listStudents = originalListStudents;
    } else {
      listStudents = originalListStudents.filter((item) =>
        item.studentName.includes(searchValue)
      );
    }

    this.setState({
      search: listStudents,
    });
  };

  componentDidUpdate(prevProps, prevState) {
    if (prevState.editingStudent !== this.state.editingStudent) {
      if (this.state.editingStudent) {
        const { editingStudent } = this.state;
        this.setState({ values: editingStudent });
      } else {
        this.setState({
          values: {
            id: "",
            studentName: "",
            phoneNumber: "",
            email: "",
          },
          editingStudent: null,
          search: null,
        });
      }
    }
  }

  handleStartEdit = (idStudent) => {
    const foundStudent = this.state.listStudents.find((item) => {
      return item.id === idStudent;
    });
    this.setState({ editingStudent: foundStudent });
  };

  handleEdit = (event) => {
    event.preventDefault();

    const foundStudentIndex = this.state.listStudents.findIndex((item) => {
      return item.id === this.state.editingStudent.id;
    });

    const newListStudents = [...this.state.listStudents];
    newListStudents[foundStudentIndex] = this.state.values;
    this.setState({ listStudents: newListStudents, editingStudent: null });
  };

  handleDelete = (idStudent) => {
    const foundStudentIndex = this.state.listStudents.findIndex((item) => {
      return item.id === idStudent;
    });

    if (foundStudentIndex !== -1) {
      const newListStudents = [...this.state.listStudents];
      newListStudents.splice(foundStudentIndex, 1);
      this.setState({ listStudents: newListStudents, editingStudent: null });
    }
  };

  render() {
    let { values, errors, listStudents, search, editingStudent } = this.state;

    let { id, studentName, phoneNumber, email } = values;

    return (
      <div className="container">
        <h4 className="bg-dark text-white py-2 px-3">Thông tin sinh viên</h4>
        <form>
          <div className="row">
            <div className="col-6 mb-4">
              <label htmlFor="">Mã SV</label>
              <input
                value={id}
                type="text"
                id="id"
                name="id"
                className="form-control"
                pattern="^[a-zA-Z0-9]*$"
                onChange={this.handleCheckValid}
                onBlur={this.handleCheckValid}
                disabled={editingStudent}
              />
              {errors.id && (
                <span className="text text-danger">{errors.id}</span>
              )}
            </div>
            <div className="col-6  mb-4">
              <label htmlFor="">Họ tên</label>
              <input
                value={studentName}
                type="text"
                id="studentName"
                name="studentName"
                className="form-control"
                onChange={this.handleCheckValid}
                onBlur={this.handleCheckValid}
              />
              {errors.studentName && (
                <span className="text text-danger">{errors.studentName}</span>
              )}
            </div>
            <div className="col-6  mb-4">
              <label htmlFor="">Số điện thoại</label>
              <input
                value={phoneNumber}
                type="text"
                id="phoneNumber"
                name="phoneNumber"
                className="form-control"
                pattern="(84|0[3|5|7|8|9])+([0-9]{8})\b"
                onChange={this.handleCheckValid}
                onBlur={this.handleCheckValid}
              />
              {errors.phoneNumber && (
                <span className="text text-danger">{errors.phoneNumber}</span>
              )}
            </div>
            <div className="col-6  mb-4">
              <label htmlFor="">Email</label>
              <input
                value={email}
                type="text"
                id="email"
                name="email"
                className="form-control"
                pattern="^[\w-\.]+@([\w-]+\.)+[\w-]{2,4}$"
                onChange={this.handleCheckValid}
                onBlur={this.handleCheckValid}
              />
              {errors.email && (
                <span className="text text-danger">{errors.email}</span>
              )}
            </div>
            <div className="col-12">
              {editingStudent ? (
                <button
                  type="button"
                  onClick={this.handleEdit}
                  className="btn btn-warning"
                >
                  Cập nhật
                </button>
              ) : (
                <button onClick={this.handleSubmit} className="btn btn-success">
                  Thêm sinh viên
                </button>
              )}
            </div>
          </div>
        </form>
        <div className="form-group row mt-5">
          <label className="col-2 col-form-label">
            <i className="fa fa-search"></i> Tìm kiếm
          </label>
          <div className="col-10">
            <input
              type="text"
              className="form-control"
              name="search"
              id="search"
              placeholder="Nhập tên sinh viên"
              onChange={this.handleSearch}
              onBlur={this.handleSearch}
            />
          </div>
        </div>
        <Student_List
          data={listStudents}
          search={search}
          handleDelete={this.handleDelete}
          handleStartEdit={this.handleStartEdit}
        />
      </div>
    );
  }
}
